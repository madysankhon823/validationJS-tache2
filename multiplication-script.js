const form = document.getElementById('form');
const valeur1 = document.getElementById('Valeur1');
const valeur2 = document.getElementById('Valeur2');
const suit = document.getElementById('suite');
const table = document.getElementById("table");
let erreur = document.getElementById("erreur");
suit.style.display = "none";

document.getElementById("Btn").addEventListener("click", creerTableau);
function creerTableau(e) {
	e.preventDefault();
	let input1Value = valeur1.value;
	let input2Value = valeur2.value;
	let m = Number(input1Value),
		n = Number(input2Value);

	if (Number.isInteger(m) && Number.isInteger(n)) {
		if (m === 0 || n === 0) {
			erreur.innerHTML = "Saississez des valeurs numeriques differentes de 0";
			setTimeout(() => {
				erreur.innerHTML = "";
				valeur1.value = "";
				valeur2.value = "";
			}, 5000);
		} else if (valeur1.value === "" || valeur2.value === "") {
			erreur.innerHTML =
				"Au moin un des champs de saisie est vide, saississez des valeurs numeriques differentes de 0";
			setTimeout(() => {
				erreur.innerHTML = "";
				valeur1.value = "";
				valeur2.value = "";
			}, 5000);
		} else {
			let resultat;
			suit.style.display = "block";
			let thead = table.firstChild.nextSibling;
			let tbody = thead.nextElementSibling;
			for (let i = 0; i <= n; i++) {
                if (i === 0) {
                    thead.appendChild(document.createElement("th")).innerHTML = ` `;
                }
                else {
                    thead.appendChild(document.createElement("th")).innerHTML = `colonne ${i}`;
                }
			}
			for (let i = 1; i <= m; i++) {
				let line = tbody.appendChild(document.createElement("tr"));
				console.log(`La ligne ${i}`);
				for (let j = 0; j <= n; j++) {
                    if (j === 0) {
                        let column = line.appendChild(document.createElement("th"));
					    column.innerHTML = `Ligne ${i}`;
                    }
                    else {
                        let column = line.appendChild(document.createElement("td"));
					    column.innerHTML = i * j;
					    resultat = i * j;
                    }
					console.log(resultat);
				}
			}
            document.getElementById("Btn").disabled = true;
		}
	} else {
		erreur.innerHTML =
			"Au moin un des champs de saisie n'est pas un nombre, saississer des nombres ou chiffres";
		setTimeout(() => {
			erreur.innerHTML = "";
			valeur1.value = "";
			valeur2.value = "";
		}, 2000);
	}
}